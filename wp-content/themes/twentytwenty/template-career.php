<?php
/*Template Name: career*/
get_header(); ?>
<main id="site-content">
   <div class="container-fluid">
   <?php query_posts(array(
      'post_type' => 'career'
   ));


   while (have_posts()) : the_post(); ?>
      <div class="cpost">
         <div class="image">
            <?php the_post_thumbnail(); ?>
         </div>
         <div class="content-div">
            <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
            <p><?php the_excerpt(); ?></p>
         </div>
</div>
   <?php endwhile; ?>
   </div>
</main>
<?php
get_footer();
?>