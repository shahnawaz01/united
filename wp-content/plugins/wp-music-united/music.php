<?php
/*
Plugin Name: Music United
Plugin URI: 
Description: 
Version: 1.0
Author: Shahnawaz
Author URI: 
License: 
*/

add_action( 'init', 'create_music' );


function create_music() {
    register_post_type( 'music_post_united',
        array(
            'labels' => array(
                'name' => 'Music',
                'singular_name' => 'Music',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Music',
                'edit' => 'Edit',
                'edit_item' => 'Edit Music',
                'new_item' => 'New Music',
                'view' => 'View',
                'view_item' => 'View Music',
                'search_items' => 'Search Music',
                'not_found' => 'No Music found',
                'not_found_in_trash' => 'No Music found in Trash',
                'parent' => 'Parent Music'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail', 'custom-fields' ),
            'taxonomies' => array( 'category' ),
            'menu_icon' => plugins_url( 'images/image.png', __FILE__ ),
            'has_archive' => true
        )
    );
}
add_action( 'admin_init', 'my_admin' );
function my_admin() {
    add_meta_box( 'music_met_meta_box',
        'Music Details',
        'display_music_met_meta_box',
        'music_post_united', 'normal', 'high'
    );
}

function display_music_met_meta_box( $music_met ) {
    $composer_name = esc_html( get_post_meta( $music_met->ID, 'composer_name', true ) );
    $year_recording = esc_html( get_post_meta( $music_met->ID, 'year_recording', true ) );
    $contributors = esc_html( get_post_meta( $music_met->ID, 'contributors', true ) );
    $url = esc_html( get_post_meta( $music_met->ID, 'url', true ) );
    $price = esc_html( get_post_meta( $music_met->ID, 'price', true ) );
    $publisherss = esc_html( get_post_meta( $music_met->ID, 'publisherss', true ) );
    ?>
    <table>
        <tr>
            <td style="width: 100%">Composer Name</td>
            <td><input type="text" size="80" name="music_met_composer_name" value="<?php echo $composer_name; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 100%">Publisher</td>
            <td><input type="text" size="80" name="music_met_publisherss" value="<?php echo $publisherss; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 100%">Year Recording</td>
            <td><input type="text" size="80" name="music_met_year_recording" value="<?php echo $year_recording; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 100%">Contributors</td>
            <td><input type="text" size="80" name="music_met_contributors" value="<?php echo $contributors; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 100%">URL</td>
            <td><input type="text" size="80" name="music_met_url" value="<?php echo $url; ?>" /></td>
        </tr>
        <tr>
            <td style="width: 100%">Price</td>
            <td><input type="text" size="80" name="music_met_price" value="<?php echo $price; ?>" /></td>
        </tr>
        
    </table>
    <?php
}
add_action( 'save_post', 'add_music_fields', 10, 2 );

function add_music_fields( $music_id, $music_det ) {
    // Check post type for movie reviews
    if ( $music_det->post_type == 'music_post_united' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['music_met_composer_name'] ) && $_POST['music_met_composer_name'] != '' ) {
            update_post_meta( $music_id, 'composer_name', $_POST['music_met_composer_name'] );
        }
        if ( isset( $_POST['music_met_publisherss'] ) && $_POST['music_met_publisherss'] != '' ) {
            update_post_meta( $music_id, 'publisherss', $_POST['music_met_publisherss'] );
        }
        if ( isset( $_POST['music_met_year_recording'] ) && $_POST['music_met_year_recording'] != '' ) {
            update_post_meta( $music_id, 'year_recording', $_POST['music_met_year_recording'] );
        }
        if ( isset( $_POST['music_met_contributors'] ) && $_POST['music_met_contributors'] != '' ) {
            update_post_meta( $music_id, 'contributors', $_POST['music_met_contributors'] );
        }
        if ( isset( $_POST['music_met_url'] ) && $_POST['music_met_url'] != '' ) {
            update_post_meta( $music_id, 'url', $_POST['music_met_url'] );
        }
        if ( isset( $_POST['music_met_price'] ) && $_POST['music_met_price'] != '' ) {
            update_post_meta( $music_id, 'price', $_POST['music_met_price'] );
        }
    }
}


add_filter( 'template_include', 'include_template_function', 1 );

function include_template_function( $template_path ) {
    if ( get_post_type() == 'music_post_united' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array ( 'single-music_post_united.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) . '/single-music_post_united.php';
            }
        }
    }
    return $template_path;
}
?>