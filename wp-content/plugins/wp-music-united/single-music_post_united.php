<?php
 /*Template Name: New Template
 */
 
get_header(); ?>
<div id="primary">
    <div id="content" role="main">
    <?php
    $mypost = array( 'post_type' => 'music_post_united', );
    $loop = new WP_Query( $mypost );
    ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post();?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <header class="entry-header">
 
                <!-- Display featured image in right-aligned floating div -->
                <div style="float: right; margin: 10px">
                    <?php the_post_thumbnail( array( 100, 100 ) ); ?>
                </div>

                <!-- Display movie review contents -->
                <div class="entry-content"><?php the_content(); ?>
 
                <!-- Display Title and Author Name -->
                <p><strong>Title: </strong><?php the_title(); ?><br /></p>
                <p><strong>Composer Name: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), 'composer_name', true ) ); ?></p>
                <br />
                <p><strong>Publisher: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), 'publisherss', true ) ); ?></p>
                <br />
                <p><strong>Year Recording: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), 'year_recording', true ) ); ?></p>
                <br />
                <p><strong>Contributors: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), 'contributors', true ) ); ?></p>
                <br />
                <p><strong>URL: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), 'url', true ) ); ?></p>
                <br />
                <p><strong>Price: </strong>
                <?php echo esc_html( get_post_meta( get_the_ID(), 'price', true ) ); ?></p>
                <br />
                </div>
                
            </header>
 
            
        </article>
 
    <?php endwhile; ?>
    </div>
</div>
<?php wp_reset_query(); ?>
<?php get_footer(); ?>